provider "google" {
  project = var.project
  region  = var.region
}

# recommend newest version of Terraform, however this is tested working on 0.12 - 1.0
terraform {
  required_version = ">= 0.12"
}

# instructions regarding backend will be added to readme
# currently, backend is defined and provisioned into GCP using ArgoCD
# terraform {
#   backend "gcs" {
#     bucket  = "gitlab-runner-backend"
#     prefix  = "terraform/state"
#   }
# }

resource "google_compute_address" "gitlab_runner_external_address" {
  name         = "gitlab_runner_external_address"
  address_type = "EXTERNAL"
  region       = var.region
}


# Service account for Gitlab Runner 
resource "google_service_account" "gitlab_runner" {
  project = var.project
  account_id = var.runner_account
  display_name = var.runner_display_name
}


# Instance
resource "google_compute_instance" "gitlab_ci_runner" {
  project = var.project
  name = var.instance_name
  machine_type = var.instance_type
  zone = var.zone
  tags = ["https-server", "http-server"]


    boot_disk {
    initialize_params {
      # set image to data section at bottom of file
      image = data.google_compute_image.ubuntu.self_link
    }
  }


    network_interface {
        network = "default"

    access_config {
      # external IP
      nat_ip = google_compute_address.gitlab_runner_external_address.address
    }
  }


    metadata = {
    startup-script = data.template_file.deployment_shell_script.rendered
    sshKeys        = "ubuntu:${file(var.public_key_file)}"
  }


  provisioner "remote-exec" {
    inline = [
      "echo 'Scheduling instance reboot in one minute ...'",
      "sudo shutdown -r +1",
    ]


    connection {
      host        = google_compute_instance.gitlab_ci_runner.network_interface[0].access_config[0].nat_ip
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(var.private_key_file)
      timeout     = "5m"
    }
  }
  depends_on = [
    google_compute_address.pritunl-address,
  ]
}



# instance image type
data "google_compute_image" "ubuntu" {
  family  = "ubuntu-2004-lts"
  project = "ubuntu-os-cloud"
}