variable "region" {
    description = "GCP region for Gitlab Runner VM"
    default = "us-central1"
}

variable "zone" {
    description = "GCP zone for Gitlab Runner VM"
    default = "us-central1-a"
}

variable "project" {
    description = "GCP project ID"
    default = "example-project-name"
}

variable "runner_account" {
    description = "account_id name for the Gitlab Runner service account"
    default = "gitlab-runner"
}

variable "runner_display_name" {
    description = "display name for Gitlab Runner service account"
    default = "Gitlab Runner"
}

variable "instance_name" {
    description = "name for instance"
    default = "gitlab-runner"
}

variable "instance_type" {
    description = "GCP machine type for instance"
    # default = "f1-micro"
    default = "n1-standard-1"
}

variable "private_key_file" {
    description = "--- change --- path and private key name to point to the key you want added as metadata on GCE instance"
    default = "~/.ssh/placeholderkeyname"   # see readme for key generation commands
}
